package com.pens.favian.intentlanjutan;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class ImplicitIntentActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btnMcDonald = (Button)findViewById(R.id.btnMcDonald);
        Button btnKFC = (Button)findViewById(R.id.btnKFC);
        Button btnBelajarAndroid = (Button)findViewById(R.id.btnBelajarAndroid);
        Button btnJadwalPuasa = (Button)findViewById(R.id.btnJadwalPuasa);
        Button btnKamera = (Button)findViewById(R.id.btnKamera);

        btnMcDonald.setOnClickListener(new Button.OnClickListener(){
            public void onClick(View view){
                CallIntent(view);
            }
        });
        btnKFC.setOnClickListener(new Button.OnClickListener(){
            public void onClick(View view){
                CallIntent(view);
            }
        });
        btnBelajarAndroid.setOnClickListener(new Button.OnClickListener(){
            public void onClick(View view){
                CallIntent(view);
            }
        });
        btnJadwalPuasa.setOnClickListener(new Button.OnClickListener(){
            public void onClick(View view){
                CallIntent(view);
            }
        });
        btnKamera.setOnClickListener(new Button.OnClickListener(){
            public void onClick(View view){
                CallIntent(view);
            }
        });
    }

    public void CallIntent(View view){
        Intent intent = null;
        switch (view.getId()){
            case R.id.btnKFC: intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:14022"));
                startActivity(intent);
                break;
            case R.id.btnMcDonald: intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:14045"));
                startActivity(intent);
                break;
            case R.id.btnBelajarAndroid: intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://agusharyanto.net"));
                startActivity(intent);
                break;
            case R.id.btnJadwalPuasa: intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://pkpu.or.id/imsyak/"));
                startActivity(intent);
                break;
            case R.id.btnKamera: intent = new Intent("android.media.action.IMAGE_CAPTURE");
                startActivityForResult(intent, 0);
                break;
                default:
                    break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK && requestCode == 0) {
            String result = data.toURI();
            Toast.makeText(this, result, Toast.LENGTH_LONG);
        }
    }
}